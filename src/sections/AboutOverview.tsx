import styles from '@local-styles/About.module.scss';

import PersonalInfo from '../components/about/PersonalInfo';
import Skills from '../components/about/Skills';
import WelcomeText from '../components/about/WelcomeText';

export default function AboutOverview() {
  return (
    <section className={styles.aboutSection}>
      <WelcomeText />
      <PersonalInfo />
      <Skills />
    </section>
  );
}
