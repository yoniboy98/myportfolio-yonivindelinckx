import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '@local-styles/Education.module.scss';

import { Record } from '../data';

interface AchievementCardsProps {
  records: Record[];
}

export default function AchievementCards({ records }: AchievementCardsProps) {
  return (
    <div className={styles.timeline}>
      {records.map((record: Record) => (
        <div key={record.id}>
          <h4 className={styles.timeTitle}>{record.title}</h4>
          {record.description ? (
            <p className={styles.timelineText}>{record.description}</p>
          ) : null}
          {record.options ? (
            <p className={styles.timelineOptions}>
              <span>Option(s):</span> {record.options}
            </p>
          ) : null}
          {record.location ? (
            <p className={styles.timelineText}>
              <span>Location:</span> {record.location}
            </p>
          ) : null}
          <h6 className={styles.timelineDate}>
            <FontAwesomeIcon icon={faCalendar} /> Achieved at: {record.date}
          </h6>
          <div className={styles.underlineDecorator}></div>
        </div>
      ))}
    </div>
  );
}
