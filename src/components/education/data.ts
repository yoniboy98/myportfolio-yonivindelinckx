import { v7 as uuidv7 } from 'uuid';

export type Record = {
  id: string;
  title: string;
  date: string;
  options?: string;
  description?: string;
  location?: string;
};

export const degrees: Record[] = [
  {
    id: uuidv7(),
    title: 'Bachelor in Applied Computer Science',
    options: 'Software Development, Web And Mobile Development & Networking',
    location: `Odisee Brussel`,
    date: '27/06/2022',
  },
  {
    id: uuidv7(),
    title: 'Associate Degree in Computer Science',
    options: 'Programming',
    location: `Odisee Brussel`,
    date: '25/06/2020',
  },
];

export const certifications: Record[] = [
  {
    id: uuidv7(),
    title: 'Microsoft Technology Associate',
    description: ` 98-388: MTA: Introduction to Programming using Java`,
    date: '19/06/2019',
  },
  {
    id: uuidv7(),
    title: 'Microsoft Technology Associate',
    description: `98-366: MTA: Networking Fundamentals`,
    date: '04/04/2019',
  },
  {
    id: uuidv7(),
    title: 'Microsoft Office Specialist',
    description: `77-727: MOS: Microsoft Office Excel 2016: Core Data Analysis,
          Manipulation, and Presentation`,
    date: '11/10/2018',
  },
];
