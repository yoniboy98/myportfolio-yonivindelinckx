import { render, screen } from '@testing-library/react';

import WelcomeText from '../WelcomeText';

jest.mock(
  '@local-assets/cv/CV_Inetum_Realdolmen_EN_VINDELINCKX_Yoni-5.pdf',
  () => 'cv.png',
);

it('Renders the welcome text donwload cv link', () => {
  render(<WelcomeText />);
  const cvDownloadLink = screen.getByTestId('cvDownloadLink');
  expect(cvDownloadLink).toHaveAttribute('href');
  expect(cvDownloadLink).toHaveAttribute('target');
  expect(cvDownloadLink).toBeInTheDocument();
});
