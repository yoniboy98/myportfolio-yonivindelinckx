import { render, screen } from '@testing-library/react';

import ProfileTitle from '../ProfileTitle';

jest.mock('@local-assets/profilePicture.png', () => 'profilePicture.png');

it('Renders profile title text', () => {
  render(<ProfileTitle />);
  const profileTitle = screen.getByText(
    'Web and Mobile Developer at Inetum-Realdolmen',
  );
  expect(profileTitle).toBeInTheDocument();
});

it('Renders the profile image', () => {
  render(<ProfileTitle />);
  const imgElement = screen.getByAltText('profile');
  expect(imgElement).toBeInTheDocument();
  expect(imgElement).toHaveAttribute('src', 'profilePicture.png');
});
