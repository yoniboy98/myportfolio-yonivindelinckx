import profilePicture from '@local-assets/profilePicture.png';
import profilePicturePlaceholder from '@local-assets/profilePicturePlaceholder.jpg';

export default function ProfileTitle() {
  const fullName = 'Yoni Vindelinckx';
  const splittedName = fullName.split(' ');
  const firstName = splittedName[0];
  const lastName = splittedName[1];

  return (
    <>
      <img src={profilePicture ?? profilePicturePlaceholder} alt="profile" />
      <h1>
        <span>{firstName.charAt(0)}</span>
        {firstName.slice(1)} <span>{lastName.charAt(0)}</span>
        {lastName.slice(1)}
      </h1>
      <h4>Web and Mobile Developer at Inetum-Realdolmen</h4>
    </>
  );
}
