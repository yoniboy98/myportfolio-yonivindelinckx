import { render, screen } from '@testing-library/react';

import PersonalInfo from '../PersonalInfo';

it('Renders the personal info text', () => {
  render(<PersonalInfo />);
  const spanTextBirth = screen.getByText('Date of Birth:');
  const spanTextPhone = screen.getByText('Phone Number:');
  const spanTextEmail = screen.getByText('Email:');
  expect(spanTextBirth).toBeInTheDocument();
  expect(spanTextPhone).toBeInTheDocument();
  expect(spanTextEmail).toBeInTheDocument();
});
