import react from '@vitejs/plugin-react';
import browserslistToEsbuild from 'browserslist-to-esbuild';
import path, { resolve } from 'path';
import { defineConfig } from 'vite';
import viteTsconfigPaths from 'vite-tsconfig-paths';

const root = resolve(__dirname, 'src');

// https://vitejs.dev/config/
export default defineConfig({
  base: '/',
  plugins: [react(), viteTsconfigPaths()],
  server: {
    host: 'localhost',
    port: 3000,
    open: true,
  },
  resolve: {
    alias: {
      '@local-assets': path.resolve(root, 'assets'),
      '@local-styles': path.resolve(root, 'styles'),
    },
  },
  build: {
    target: browserslistToEsbuild(['>0.2%', 'not dead', 'not op_mini all']),
  },
});
