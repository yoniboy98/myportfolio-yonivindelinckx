import { getAnalytics } from 'firebase/analytics';

import { analytics, app } from '../config/firebaseConfig';

jest.mock('firebase/analytics', () => ({
  getAnalytics: jest.fn().mockReturnValue({
    logEvent: 'CV_Downloaded',
  }),
}));

describe('Firebase Analytics', () => {
  it('Should initialize analytics correctly', () => {
    expect(getAnalytics).toHaveBeenCalledWith(app);
    expect(analytics).toBeDefined();
    expect(analytics).toHaveProperty('logEvent', 'CV_Downloaded');
  });
});
