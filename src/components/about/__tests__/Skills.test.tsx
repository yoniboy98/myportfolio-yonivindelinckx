import { skills } from '../data';

test('I have 6 skilss', () => {
  expect(skills).toHaveLength(6);
});

test('Skill object has props', () => {
  skills.forEach((skill) => {
    expect(skill).toHaveProperty('id');
    expect(skill).toHaveProperty('title');
    expect(skill).toHaveProperty('logo');
    expect(skill).toHaveProperty('color');
  });
});
