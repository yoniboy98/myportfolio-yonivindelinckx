import localCV from '@local-assets/cv/CV_Inetum_Realdolmen_EN_VINDELINCKX_Yoni.pdf';
import styles from '@local-styles/About.module.scss';
import { logEvent } from 'firebase/analytics';
import { getDownloadURL, ref } from 'firebase/storage';
import { useEffect, useState } from 'react';

import { isProductionEnv } from '../../config/environment';
import { analytics, storage } from '../../config/firebaseConfig';

export default function WelcomeText() {
  const [storageFileURL, setStorageFileURL] = useState('');

  useEffect(() => {
    (async () => {
      try {
        const storageRef = ref(storage, 'files/cv_Vindelinckx_Yoni_eng.pdf');
        const url = await getDownloadURL(storageRef);
        setStorageFileURL(url);
      } catch (error) {
        if (isProductionEnv()) {
          logEvent(analytics, 'CV_Downloaded_failed', { error });
        }
      }
    })();
  }, []);

  const handleTrackCVDownload = () => {
    if (isProductionEnv()) {
      logEvent(analytics, 'CV_Downloaded');
    }
  };

  return (
    <div className={styles.aboutText}>
      <h3>
        My name is <span>Yoni Vindelinckx</span> and I specialise in improving
        user experience through effective collaborations and structured
        developments of websites and applications. My experiences have given me
        a deep understanding of how websites and applications work behind the
        scenes, allowing me to tackle challenges with enthusiasm and precision.
        I am highly committed to continuous learning and thrive on steering
        projects towards successful outcomes. If you are interested in my CV and
        want to discover how my expertise can help your organisation, please
        click
        <a
          href={storageFileURL ?? localCV}
          data-testid="cvDownloadLink"
          onClick={handleTrackCVDownload}
          download="CV_Vindelinckx_Yoni"
          target="noopener"
        >
          here
        </a>
        !
      </h3>
    </div>
  );
}
