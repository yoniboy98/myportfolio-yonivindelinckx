import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

export interface FontAwesomeIconProps {
  icon: IconDefinition;
  color?: string;
}
