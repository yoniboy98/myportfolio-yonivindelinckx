import styles from '@local-styles/Education.module.scss';

import Certifications from '../components/education/molecules/Certifications';
import Degrees from '../components/education/molecules/Degrees';

export default function Education() {
  return (
    <div className={styles.educationSection}>
      <Degrees />
      <Certifications />
    </div>
  );
}
