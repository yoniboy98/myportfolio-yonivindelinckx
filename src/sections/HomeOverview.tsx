import styles from '@local-styles/Home.module.scss';

import ProfileTitle from '../components/home/ProfileTitle';
import SocialLinks from '../components/home/SocialLinks';

export default function HomeOverview() {
  return (
    <section className={styles.homeSection}>
      <ProfileTitle />
      <SocialLinks />
    </section>
  );
}
