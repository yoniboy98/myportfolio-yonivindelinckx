import { faTrophy } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '@local-styles/Education.module.scss';

import AchievementCards from '../atoms/AchievementCards';
import { degrees } from '../data';

export default function Degrees() {
  return (
    <div className={styles.educations}>
      <h3>
        <FontAwesomeIcon
          data-testid="fa-icon-trophy"
          icon={faTrophy}
          color="#FFD700"
        />
        Degrees
      </h3>
      <AchievementCards records={degrees} />
    </div>
  );
}
