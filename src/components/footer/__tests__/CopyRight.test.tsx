import { render, screen } from '@testing-library/react';

import CopyRight from '../CopyRight';

const currentDate = new Date();

it('Renders Copyright text', () => {
  render(<CopyRight />);
  const copyrightText = screen.getByText(
    `© ${currentDate.getFullYear()} Portfolio Yoni Vindelinckx`,
  );
  expect(copyrightText).toBeInTheDocument();
});
