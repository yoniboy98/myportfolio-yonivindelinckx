import { getAnalytics } from 'firebase/analytics';
import { initializeApp } from 'firebase/app';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyBBXum6ZdR_m0hULfbzQzjrt-eSlCh5YzI',
  authDomain: 'yonivindelinckx-61c4c.firebaseapp.com',
  projectId: 'yonivindelinckx-61c4c',
  storageBucket: 'yonivindelinckx-61c4c.appspot.com',
  messagingSenderId: '632174296823',
  appId: '1:632174296823:web:91af20752349f543231df2',
  measurementId: 'G-KQ2XWXR2ML',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const analytics = getAnalytics(app);
const storage = getStorage(app);

export { analytics, app, storage };
