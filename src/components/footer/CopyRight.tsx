import styles from '@local-styles/Footer.module.scss';
import { useEffect, useState } from 'react';

export default function CopyRight() {
  const [currentYear, setCurrentYear] = useState<number>();

  useEffect(() => {
    const newCurrentYear = new Date().getFullYear();
    setCurrentYear(newCurrentYear);
  }, [currentYear]);

  return (
    <div className={styles.copyright}>
      &copy; {currentYear} Portfolio Yoni Vindelinckx
    </div>
  );
}
