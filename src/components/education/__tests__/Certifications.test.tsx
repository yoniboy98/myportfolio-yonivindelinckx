import { render, screen } from '@testing-library/react';

import { FontAwesomeIconProps } from '../../../types/FontAwesomeTypes';
import Certifications from '../molecules/Certifications';

jest.mock('@fortawesome/react-fontawesome', () => ({
  FontAwesomeIcon: ({ icon, color }: FontAwesomeIconProps) => (
    <svg data-testid={`fa-icon-${icon.iconName}`} style={{ color }} />
  ),
}));

describe('Certifications Component', () => {
  it('Renders certification component', () => {
    render(<Certifications />);
    expect(screen.getByText('Certifications')).toBeInTheDocument();
    expect(screen.getByTestId('fa-icon-medal')).toBeInTheDocument();
    expect(screen.getByTestId('fa-icon-medal')).toHaveStyle(
      'color: rgb(255, 215, 0)',
    );
  });
});
