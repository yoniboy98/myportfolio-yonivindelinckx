import {
  faAngular,
  faCss3,
  faHtml5,
  faJs,
  faNodeJs,
  faReact,
  IconDefinition,
} from '@fortawesome/free-brands-svg-icons';
import { v7 as uuidv7 } from 'uuid';

export type Skill = {
  id: string;
  title: string;
  logo: IconDefinition;
  color: string;
  subTitle?: string;
};

export const skills: Skill[] = [
  {
    id: uuidv7(),
    title: 'Angular',
    logo: faAngular,
    color: '#c3002f',
  },
  {
    id: uuidv7(),
    title: 'CSS',
    subTitle: 'Sass & SCSS',
    logo: faCss3,
    color: '#264de4',
  },
  {
    id: uuidv7(),
    title: 'HTML',
    logo: faHtml5,
    color: '#e34c26',
  },
  {
    id: uuidv7(),
    title: 'JavaScript',
    subTitle: 'TypeScript',
    logo: faJs,
    color: '#f0db4f',
  },
  {
    id: uuidv7(),
    title: 'Node.js',
    subTitle: 'Express.js, Koa.js & Fastify',
    logo: faNodeJs,
    color: '#215732',
  },
  {
    id: uuidv7(),
    title: 'React',
    subTitle: 'React Native',
    logo: faReact,
    color: '#61DBFB',
  },
];

export const gradientColors: string[] = [
  'linear-gradient(to right, #fc466b, #3f5efb)',
  'linear-gradient(to right, #005aa7, #fffde4)',
  'linear-gradient(to right, #556270, #ff6b6b)',
  'linear-gradient(to right, #b993d6, #8ca6db)',
  'linear-gradient(to right, #108dc7, #ef8e38)',
  'linear-gradient(to right, #f0c27b, #4b1248)',
];
