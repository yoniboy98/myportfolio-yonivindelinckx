import { render, screen } from '@testing-library/react';

import { FontAwesomeIconProps } from '../../../types/FontAwesomeTypes';
import SocialLinks from '../SocialLinks';

jest.mock('@fortawesome/react-fontawesome', () => ({
  FontAwesomeIcon: ({ icon }: FontAwesomeIconProps) => (
    <svg data-testid={`fa-icon-${icon.iconName}`} />
  ),
}));

it('Renders LinkedIn link with FontAwesome icon', () => {
  render(<SocialLinks />);

  const linkedinLink = screen.getByTestId('linkedin-link');
  expect(linkedinLink).toBeInTheDocument();
  expect(linkedinLink).toHaveAttribute(
    'href',
    'https://www.linkedin.com/in/yoni-vindelinckx-a69b2517a/',
  );
  const linkedinIcon = screen.getByTestId('fa-icon-linkedin');
  expect(linkedinIcon).toBeInTheDocument();
});

it('Renders Gitlab link with FontAwesome icon', () => {
  render(<SocialLinks />);

  const gitlabLink = screen.getByTestId('gitlab-link');
  expect(gitlabLink).toBeInTheDocument();
  expect(gitlabLink).toHaveAttribute(
    'href',
    'https://gitlab.com/yoniboy98/myportfolio-yonivindelinckx',
  );
  const gitlabIcon = screen.getByTestId('fa-icon-gitlab');
  expect(gitlabIcon).toBeInTheDocument();
});

it('Renders Github link with FontAwesome icon', () => {
  render(<SocialLinks />);

  const githubLink = screen.getByTestId('github-link');
  expect(githubLink).toBeInTheDocument();
  expect(githubLink).toHaveAttribute(
    'href',
    'https://github.com/yoni1998?tab=repositories',
  );
  const githubIcon = screen.getByTestId('fa-icon-gitlab');
  expect(githubIcon).toBeInTheDocument();
});
