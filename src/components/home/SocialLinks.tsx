import { IconProp } from '@fortawesome/fontawesome-svg-core';
import {
  faGithub,
  faGitlab,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '@local-styles/Home.module.scss';

export default function SocialLinks() {
  const email = 'yoni.vindelinckx@inetum-realdolmen.world';
  const emailSubject = 'Hi Yoni!';
  return (
    <div className={styles.socialLinks}>
      <a
        href="https://www.linkedin.com/in/yoni-vindelinckx-a69b2517a/"
        data-testid="linkedin-link"
        target="noopener"
      >
        <FontAwesomeIcon icon={faLinkedin as IconProp} />
      </a>
      <a
        href="https://gitlab.com/yoniboy98/myportfolio-yonivindelinckx"
        data-testid="gitlab-link"
        target="noopener"
      >
        <FontAwesomeIcon icon={faGitlab as IconProp} />
      </a>
      <a
        href="https://github.com/yoni1998?tab=repositories"
        data-testid="github-link"
        target="noopener"
      >
        <FontAwesomeIcon icon={faGithub as IconProp} />
      </a>
      <a href={`mailto:${email}?subject=${encodeURIComponent(emailSubject)}`}>
        <FontAwesomeIcon icon={faEnvelope} />
      </a>
    </div>
  );
}
