import CopyRight from './components/footer/CopyRight';
import AboutOverview from './sections/AboutOverview';
import EducationOverview from './sections/EducationOverview';
import HomeOverview from './sections/HomeOverview';

const App = () => {
  return (
    <>
      <HomeOverview />
      <AboutOverview />
      <EducationOverview />
      <CopyRight />
    </>
  );
};

export default App;
