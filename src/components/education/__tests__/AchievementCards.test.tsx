import { certifications, degrees } from '../data';

test('I have 3 certifications and 2 degrees', () => {
  expect(certifications).toHaveLength(3);
  expect(degrees).toHaveLength(2);
});

test('Certification object has props', () => {
  certifications.forEach((certification) => {
    expect(certification).toHaveProperty('id');
    expect(certification).toHaveProperty('title');
    expect(certification).toHaveProperty('description');
    expect(certification).toHaveProperty('date');
  });
});

test('Degree object has props', () => {
  degrees.forEach((degree) => {
    expect(degree).toHaveProperty('id');
    expect(degree).toHaveProperty('title');
    expect(degree).toHaveProperty('options');
    expect(degree).toHaveProperty('location');
    expect(degree).toHaveProperty('date');
  });
});
