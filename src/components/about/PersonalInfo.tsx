import styles from '@local-styles/About.module.scss';

export default function PersonalInfo() {
  return (
    <div className={styles.personalInfo}>
      <p>
        <span>Date of Birth:</span> 08/05/1998
      </p>
      <p>
        <span>Phone Number:</span> +32 470 56 53 78
      </p>
      <p>
        <span>Email:</span> yoni.vindelinckx@hotmail.com
      </p>
    </div>
  );
}
