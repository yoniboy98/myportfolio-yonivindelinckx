import { faMedal } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '@local-styles/Education.module.scss';

import AchievementCards from '../atoms/AchievementCards';
import { certifications } from '../data';

export default function Certifications() {
  return (
    <div className={styles.educations}>
      <h3>
        <FontAwesomeIcon
          data-testid="fa-icon-medal"
          icon={faMedal}
          color="#FFD700"
        />
        Certifications
      </h3>
      <AchievementCards records={certifications} />
    </div>
  );
}
