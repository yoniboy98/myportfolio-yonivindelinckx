import { render, screen } from '@testing-library/react';

import { FontAwesomeIconProps } from '../../../types/FontAwesomeTypes';
import Degrees from '../molecules/Degrees';

jest.mock('@fortawesome/react-fontawesome', () => ({
  FontAwesomeIcon: ({ icon, color }: FontAwesomeIconProps) => (
    <svg data-testid={`fa-icon-${icon.iconName}`} style={{ color }} />
  ),
}));

describe('Degrees Component', () => {
  it('Renders degree component', () => {
    render(<Degrees />);
    expect(screen.getByText('Degrees')).toBeInTheDocument();
    expect(screen.getByTestId('fa-icon-trophy')).toBeInTheDocument();
    expect(screen.getByTestId('fa-icon-trophy')).toHaveStyle(
      'color: rgb(255, 215, 0)',
    );
  });
});
