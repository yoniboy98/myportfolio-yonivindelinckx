## My personal portfolio

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

### `npm run test` or `npm run test:coverage`

Launches the test runner.

### `npm run lint`

Launches the linter check.

### `npm run ts-check`

Launches the ts check.

### `npm run build`

Builds the app for production to the `dist` folder.\
It correctly bundles the production mode and optimizes the build for the best performance.
