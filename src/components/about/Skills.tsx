import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '@local-styles/About.module.scss';

import { gradientColors, Skill, skills } from './data';

export default function Skills() {
  const randomGradientColor = Math.floor(Math.random() * 5).toFixed(0);

  const skillsCardStyle = {
    margin: '5% 1%',
    background: gradientColors[randomGradientColor as unknown as number],
    padding: '2rem',
    borderRadius: '20%',
    width: '13rem',
    boxShadow: '3px 0px 18px -3px rgba(0, 0, 0, 0.75)',
  };

  return (
    <div className={styles.skillsContainer}>
      {skills.map((skills: Skill) => (
        <div key={skills.id} style={skillsCardStyle}>
          <div className={styles.cardContent}>
            <FontAwesomeIcon
              size="6x"
              color={skills.color}
              icon={skills.logo as IconProp}
            />
            <h3>{skills.title}</h3>
            {skills.subTitle ? <h5>({skills.subTitle})</h5> : null}
          </div>
        </div>
      ))}
    </div>
  );
}
